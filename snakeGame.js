document.write("<script language='javascript'src='Queue.js'></script>")

function randomNum(min,max){
	return Math.round(min+Math.random()*(max-min));
}
// function msgHandler() {
// 	//1、取得消息体
// 	var msg = queue.pop();//undefined
// 	if (!gamePause){
// 		//2、判断消息是什么消息
// 		//3、对应不同的消息做出相应的反应
// 		if (msg.type === 'test'){
// 			console.log('test');
// 		}
// 	}
// 	//重新启动
// 	msgHandle = setTimeout('msgHandler()',0);
// }

function Snake(game){
	this.game = game
	this.body=[]
	this.food=[]
	this.currDirection='right'
	this.init=()=>{
		let i=0;
		for(;i<5;i++){
			this.body.push({x:1+i,y:1})
		}
		//转个弯
		this.body.push({x:1+i,y:1})
		this.draw()
		this.food.push({x:randomNum(0,this.game.colNum-1),y:randomNum(0,this.game.rowNum-1)})
		this.drawFood()
	}
	this.drawFood=()=>{
		for(let i=0;i<this.food.length;i++){
			let tmp = this.food[i]
			if(!this.checkFood(i)){
				this.game.gameBox[tmp.y][tmp.x].type=3
			}else {//false
				this.food.push({x:randomNum(0,this.game.colNum-1),y:randomNum(0,this.game.rowNum-1)})
				this.drawFood()
			}
		}
	}
	this.newFood=()=>{

	}
	this.checkFood=(idx)=>{
		let f = this.food[idx]
		for(let j=0;j<this.body.length;j++){
			let tmpBody = this.body[j]
			if(f.x === tmpBody.x && f.y === tmpBody.y){
				this.food.splice(idx,1)
				return true
			}
		}
		//还没有找到啦，就false
		return false
	}
	this.clearFood=()=>{
		for(let i=0;i<this.food.length;i++){
			let tmp = this.food[i]
			this.game.gameBox[tmp.y][tmp.x].type=0
		}
	}
	this.eat=(pos)=>{
		// let tmpHead=this.body.pop()
		// let tmpFood=this.food[0]
		// console.log(tmpFood)
		// console.log(tmpHead)
		// if(tmpHead.x === tmpFood.x && tmpHead.y === tmpFood.y){
		// 	console.log('吃到了')
		// }else {
		// 	console.log('没吃到')
		// }

		let head = this.body[this.body.length-1]
		for(let i=0;i<this.food.length;i++){
			let tmp = this.food[i]
			if(head.x===tmp.x && head.y===tmp.y){
				//吃
				this.body.unshift(tmp)
				tmp.x = pos.x
				tmp.y = pos.y
				this.food.splice(i,1)
				i--
				this.game.msgQueue.push({type:'plus'})
				this.food.push({x:randomNum(0,this.game.colNum-1),y:randomNum(0,this.game.rowNum-1)})
				this.drawFood()
			}
		}

	}
	this.clear=()=>{
		for(let j=0;j<this.body.length;j++){
			let tmp = this.body[j]
			this.game.gameBox[tmp.y][tmp.x].type=0
		}
	}
	this.draw=()=>{
		//把snake画到gamebox里
		for(let j=0;j<this.body.length-1;j++){
			let tmp = this.body[j]
			if(this.game.gameBox[tmp.y][tmp.x].type===0)
				this.game.gameBox[tmp.y][tmp.x].type=1
		}
		//画蛇头
		let head = this.body[this.body.length-1]
		this.game.gameBox[head.y][head.x].type=4
	}
	this.move=()=>{
//			console.log('------')
		//判断可否移动，是否gameover
		//clear
		this.clear()
		//判断game over
		let headPos = {x:this.body[this.body.length-1].x,y:this.body[this.body.length-1].y}
		//预移动
		if(this.currDirection==='down'){
			//头部向下移动
			headPos.y+=1
		}else if(this.currDirection==='left'){
			headPos.x-=1
		}else if(this.currDirection==='right'){
			headPos.x+=1
		}else{
			headPos.y-=1
		}
		if(this.isGameOver(headPos.x,headPos.y)){
			this.game.isGameStart=false
			if(headPos.y>=0&& headPos.x>=0 && headPos.y<this.game.rowNum && headPos.x<this.game.colNum)
				this.game.gameBox[headPos.y][headPos.x].type=2//红色
			if(this.game.gameOverCallback)
				this.game.gameOverCallback()
		}else{
			//move
			let tail = this.body.shift()
			//eat position
			let pos = {x:tail.x,y:tail.y}
			let head = this.body.pop()
			this.body.push(tail,head)
			tail.x = head.x
			tail.y = head.y
			head.x=headPos.x
			head.y=headPos.y
			//吃食物
			this.eat(pos)//吃口空气也是吃
		}
		//draw
		this.draw()
	}
	this.isGameOver=(nx,ny)=>{//检查蛇头,坐标，方向
		for(let i=1;i<this.body.length;i++){
			let tmpBody = this.body[i]
			if(nx === tmpBody.x && ny === tmpBody.y){
				return true
			}
		}
		if(nx<0||nx>=this.game.colNum||ny<0||ny>=this.game.rowNum){
			return true
		}else {
			return false
		}
	}
}
function SnakeGame(params) {
	console.log(params)
	this.gameOverCallback=params.gameOverEvent
	this.score=0
	this.rowNum = 30
	this.colNum=50
	this.gameBox=null
	this.createFoodHandle=null
	this.snake=null
	this.gameBox=[]
	this.snakeBody=false
	this.msgQueue=new Queue()
	this.isGameStart = false//游戏开始了么？没
	this.isGamePausing = false//游戏正暂停么？是
	this.isGameRefresh = false//游戏需要重启么？没
	this.init=()=>{
		for(let i=0;i<this.rowNum;i++){
			let row = []
			for(let j=0;j<this.colNum;j++){//列
				row.push({key:i+'_'+j,type:0})
			}
			this.gameBox.push(row);
		}
	}

	//自动移动
	setInterval((game)=>{
		if(game.isGameStart && !game.isGamePausing){
			game.snake.move()
		}
	},400,this)
	this.gameStartOverHandle=()=>{//游戏开始和结束的柄
		setTimeout(this.postMessage,10)
		if(!this.isGameStart){//如果游戏没开始
			if(this.snakeBody){
				this.snake.clear()
				this.snake.clearFood()
			}
			this.score=0
			this.snake = new Snake(this)
			this.snake.init()
			this.snakeBody=true
			this.isGameStart = true//让游戏开始
			this.isGamePausing=false
		}else {//否则，游戏已经开始了或者游戏是暂停状态或者游戏需要重启
			this.isGameRefresh=true//游戏需要重启啦
			this.isGamePausing=false
			this.msgQueue.push({type:'refresh'})
		}
	}
	this.gamePauseContinueHandle=()=>{//游戏暂停和继续的柄
		if(this.isGameStart){
			if(!this.isGamePausing)	{//如果游戏开始了，且不暂停
				this.isGamePausing = true//让游戏暂停
			}else {
				this.isGamePausing = false//让游戏继续
			}
		}
	}
	this.gameRefresh=()=>{
		if(this.isGameRefresh){
			this.isGameStart=false//让游戏结束
			console.log('重新开始')
		}
	}
	this.postMessage=()=>{
		var msg = this.msgQueue.pop()
		if(msg && this.isGameStart && !this.isGamePausing){
			if (msg.type === 'down'&& this.snake.currDirection !=='up'){
				this.snake.currDirection = 'down'
				this.snake.move()
			}else if (msg.type === 'up'&& this.snake.currDirection !=='down'){
				this.snake.currDirection = 'up'
				this.snake.move()
			}else if (msg.type === 'right'&& this.snake.currDirection !=='left'){
				this.snake.currDirection = 'right'
				this.snake.move()
			}else if (msg.type === 'left' && this.snake.currDirection !=='right'){
				this.snake.currDirection = 'left'
				this.snake.move()
			}else if (msg.type === 'plus'){
				this.score++
			}else if (msg.type === 'refresh'){
				this.gameRefresh()
			}
		}
		setTimeout(this.postMessage,10)
	}
}

